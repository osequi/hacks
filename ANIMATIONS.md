# Animations

## Framer Motion

- very complex features like gesture control and so on
- syntax is React-like

## React Spring

- the syntax is so complicated. I couldn't create a simple keyframes transitions like at https://css-tricks.com/how-css-perspective-works/?utm_source=CSS-Weekly&utm_campaign=Issue-427&utm_medium=email
